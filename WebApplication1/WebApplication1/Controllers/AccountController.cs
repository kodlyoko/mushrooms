﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Mushrooms.Models;
using Microsoft.EntityFrameworkCore;

namespace Mushrooms.Controllers
{
    
    public class AccountController : Controller
    {
        private UserManager<AppUser> userManager;
        private SignInManager<AppUser> singInManager;
        private ApplicationDbContext db;


        public AccountController(UserManager<AppUser> userManage, SignInManager<AppUser> inManager, ApplicationDbContext Db)
        {
            singInManager = inManager;
            userManager = userManage;
            db = Db;
        }
        [Authorize]
        public IActionResult Index()
        {
            var xN = HttpContext.User.Identity.Name;
            var AcheiveAll = db.Acheivements.Include("AcheivementUsers").Select(x=>x).Where(x => x.AcheivementUsers.Any(c => c.User.Username == xN)).ToList();
            var listOfA = new List<string>();
            if(AcheiveAll != null)
            {

                foreach(var item in AcheiveAll)
                {
                    listOfA.Add(item.Name);
                }
                if(listOfA==null)
                {
                    listOfA.Add("Nie znaleziono osiągnięć");
                }

            }
            ViewBag.Titles = listOfA;
            List<Report> reportsforuser = db.Reports.Select(x => x).Where(x => x.User.Username == xN).ToList();
            

            return View(reportsforuser);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var k = db.Reports.FirstOrDefault(x => x.ReportID == id);
            return View(k); 
        }
        [HttpPost]
        public async Task<IActionResult> Edit(ReportViewModel model)
        {
           // var k = db.Reports.FirstOrDefault(x => x.ReportID == model.);
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var report = await db.Reports.FindAsync(id);
            if (report == null)
            {
                return RedirectToAction(nameof(Index));
            }

            try
            {
                db.Reports.Remove(report);
                await db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {
                //Log the error (uncomment ex variable name and write a log.)
                return RedirectToAction(nameof(Index), new { id = id, saveChangesError = true });
            }   
        }
        [AllowAnonymous]
        public IActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();

        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel details, string returnUrl)
        {
            if(returnUrl == null)
            {
                string nowy = returnUrl ?? "~/";
                returnUrl = nowy;
            }

            if (ModelState.IsValid)
            {
                AppUser userActual = await userManager.FindByEmailAsync(details.Email);
                if (userActual != null)
                {
                    await singInManager.SignOutAsync();
                    Microsoft.AspNetCore.Identity.SignInResult signIn = await singInManager.PasswordSignInAsync(userActual.UserName, details.Password, false, false);
                    if (signIn.Succeeded)
                    {
                        return LocalRedirect(returnUrl);
                    }
                }
            }
            ModelState.AddModelError(nameof(LoginModel.Email), "Nieprawidłowa nazwa użytkownika lub hasło");
            return View(details); //not good
            //returnUrl = returnUrl ?? Url.Content("~/");

            //if (ModelState.IsValid)
            //{
            //    // This doesn't count login failures towards account lockout
            //    // To enable password failures to trigger account lockout, 
            //    // set lockoutOnFailure: true
            //    var result = await singInManager.PasswordSignInAsync(details.Email,
            //                       details.Password, false, false);
            //    if (result.Succeeded)
            //    {
            //        _logger.LogInformation("User logged in.");
            //        return LocalRedirect(returnUrl);
            //    }
            //    if (result.RequiresTwoFactor)
            //    {
            //        return RedirectToPage("./LoginWith2fa", new
            //        {
            //            ReturnUrl = returnUrl,
            //            RememberMe = Input.RememberMe
            //        });
            //    }
            //    if (result.IsLockedOut)
            //    {
            //        _logger.LogWarning("User account locked out.");
            //        return RedirectToPage("./Lockout");
            //    }
            //    else
            //    {
            //        ModelState.AddModelError(string.Empty, "Invalid login attempt.");
            //        return Page();
            //    }
            //}

            //// If we got this far, something failed, redisplay form
            //return Page();
        }

        
        public async Task<IActionResult> Logout()
        {
            await singInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Register()
        {
            if (singInManager.IsSignedIn(User))
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(CreateUserModel model)
        {
           

            if(ModelState.IsValid)
            {
                AppUser user = new AppUser
                {
                    UserName = model.Name,
                    Email = model.Email

                };
                if(model.PasswordConfirmed !=model.Password)
                {
                    ModelState.AddModelError("", "Powtórz hasło");

                    return View(model);
                }


                IdentityResult result = await userManager.CreateAsync(user, model.Password);
                if(result.Succeeded)
                {
                    var DataUser = new User
                    {
                        Email = model.Email,
                        Username = model.Name
                    };

                     var userContext = db.Users.Add(DataUser);
                   await  db.SaveChangesAsync();
                    return RedirectToAction("AccountCreated");
                }
                else 
                {
                    foreach(IdentityError error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }
            }

            return View(model);
        }
        [AllowAnonymous]
        public async Task<IActionResult> AccountCreated()
        {
            return View();

        }

    }
}