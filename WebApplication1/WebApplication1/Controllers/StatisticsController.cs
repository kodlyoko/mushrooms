﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Mushrooms.Models;
using Newtonsoft.Json;
using WebApplication1.Models;

namespace Mushrooms.Controllers
{
    public class StatisticsController : Controller
    {
        private ApplicationDbContext db;
        private UserManager<AppUser> userManager;
        private SignInManager<AppUser> singInManager;


        public StatisticsController(UserManager<AppUser> userManage, ApplicationDbContext DatabaseContext, SignInManager<AppUser> ap)
        {
            singInManager = ap;
            db = DatabaseContext;
            userManager = userManage;
        }
        private class Statics
        {
            public string name { get; set; }
            public int countedNumber { get; set; }
        }
        public IActionResult Index()
        {
           var listaVivodeship = new List<string>
            {
                 "dolnośląskie",
                 "kujawsko-pomorskie",
                 "lubelskie",
                 "lubuskie",
                 "łódzkie",
                 "małopolskie",
                 "mazowieckie",
                 "opolskie",
                 "podkarpackie",
                 "podlaskie",
                 "pomorskie",
                 "śląskie",
                 "świętokrzyskie",
                 "warmińsko-mazurskie",
                 "wielkopolskie",
                 "zachodniopomorskie",

            };

            var myStat = new List<Statics>();

            foreach (var item in listaVivodeship)
            {
                int k = db.Reports.Where(x => x.Vivodeship == item).ToList().Count();
                var obToAdd = new Statics
                {
                    name = item,
                    countedNumber = k

                };
                myStat.Add(obToAdd);

            }
            string json = JsonConvert.SerializeObject(myStat);
            ViewBag.MyStats = json;
            return View();
        }
    }
}