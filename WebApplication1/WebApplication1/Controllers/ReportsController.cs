﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mushrooms.Models;
using WebApplication1.Models;

namespace Mushrooms.Controllers
{
    [Authorize]
    public class ReportsController : Controller
    {
        private ApplicationDbContext db;
        private UserManager<AppUser> userManager;
        private SignInManager<AppUser> singInManager;
        private readonly IHostingEnvironment hostingEnvironment;


        public ReportsController(UserManager<AppUser> userManage, 
            ApplicationDbContext DatabaseContext, SignInManager<AppUser> ap, 
            IHostingEnvironment hosting)
        {
            singInManager = ap;
            db = DatabaseContext;
            userManager = userManage;
            this.hostingEnvironment = hosting;

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateReport(ReportViewModel report)
        {
            var userName = HttpContext.User.Identity.Name;
            try
            {
                if(!(report.Vivodeship.Count()).Equals(1))
                {
                    ModelState.AddModelError("", "Wybierz województwo");
                }
                if (ModelState.IsValid)
                {
                    string uniqueFileName = null;
                    //photo upload
                    if(report.Photo != null)
                    {
                       string folderwithUploads = Path.Combine(hostingEnvironment.WebRootPath, "images");
                       uniqueFileName = Guid.NewGuid().ToString() + "_" + report.Photo.FileName;
                       string filePath = Path.Combine(folderwithUploads, uniqueFileName);
                       report.Photo.CopyTo(new FileStream(filePath, FileMode.Create));
                        
                    }
                    

                    var context = HttpContext.User.Claims;
        
                    var user = db.Users.Include("AcheivementUsers").FirstOrDefault(x => x.Username == userName);
                    var chosenVivodeship = report.Vivodeship[0];
                    var reportNew = new Report
                    {
                        Date = DateTime.Now,
                        Description = report.Description,
                        Title = report.Title,
                        Vivodeship = chosenVivodeship,
                        FieldAccepted = report.FieldAccepted,
                        User =user,
                        photoPath = uniqueFileName 
                        
                    };

                    db.Reports.Add(reportNew);
                    var placeNew = new Place
                    {
                        lat = report.lat,
                        lng = report.lng,
                        Report = reportNew

                    };
                    db.Places.Add(placeNew);
                    await db.SaveChangesAsync();
                    var countedR= db.Reports.Select(x => x).
                        Where(x => x.UserId == user.UserId).ToList().Count();
        
                    if(countedR.Equals(1))
                    {
                        var ko = db.Acheivements.Select(x => x).Where(x => x.Name == "Nowicjusz").ToList();
                        var koCounted = ko.Count();
                        if (koCounted.Equals(0))
                        {
                            var newTitle = new Acheivement
                            {
                                Name = "Nowicjusz"
                            };
                            db.Acheivements.Add(newTitle);
                            await db.SaveChangesAsync();
                        }
                        var koP = db.Acheivements.Select(x => x).Where(x => x.Name == "Nowicjusz").ToList();
                        var IdAchievement = koP[0].Id;
                        var userId = user.UserId;
                        var newAchive = new AcheivementUser
                        {
                            UserId =userId,
                            AcheivementId=IdAchievement
                        };

                        user.AcheivementUsers.Add(newAchive);
                        await db.SaveChangesAsync();
                    }
                    if (countedR.Equals(10))
                    {
                        var ko = db.Acheivements.Select(x => x).Where(x => x.Name == "Nowicjusz").ToList();
                        var koCounted = ko.Count();
                        if (koCounted.Equals(0))
                        {
                            var newTitle = new Acheivement
                            {
                                Name = "Znawca"
                            };
                            db.Acheivements.Add(newTitle);
                            await db.SaveChangesAsync();
                        }
                        var koP = db.Acheivements.Select(x => x).Where(x => x.Name == "Nowicjusz").ToList();
                        var IdAchievement = koP[0].Id;
                        var userId = user.UserId;
                        var newAchive = new AcheivementUser
                        {
                            UserId = userId,
                            AcheivementId = IdAchievement
                        };

                        user.AcheivementUsers.Add(newAchive);
                        await db.SaveChangesAsync();
                    }

                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException /* ex */)
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }


            return RedirectToAction("Index");

        }
        [HttpGet]
        public IActionResult CreateReport()
        {
            var viewModelPass = new ReportViewModel();
            viewModelPass.Vivodeship = new List<string>
            {
                 "dolnośląskie",
                 "kujawsko-pomorskie",
                 "lubelskie",
                 "lubuskie",
                 "łódzkie",
                 "małopolskie",
                 "mazowieckie",
                 "opolskie",
                 "podkarpackie",
                 "podlaskie",
                 "pomorskie",
                 "śląskie",
                 "świętokrzyskie",
                 "warmińsko-mazurskie",
                 "wielkopolskie",
                 "zachodniopomorskie",

            };

            return View(viewModelPass);
        }
        public IActionResult Index()
        {
            var Vivodeship = new List<string>
            {
                 "dolnośląskie",
                 "kujawsko-pomorskie",
                 "lubelskie",
                 "lubuskie",
                 "łódzkie",
                 "małopolskie",
                 "mazowieckie",
                 "opolskie",
                 "podkarpackie",
                 "podlaskie",
                 "pomorskie",
                 "śląskie",
                 "świętokrzyskie",
                 "warmińsko-mazurskie",
                 "wielkopolskie",
                 "zachodniopomorskie",

            };
            ViewBag.Vivo = Vivodeship;

            var xd = User.Identity.Name;
            var user = HttpContext.User.Identity.Name;
            var reportsNow = db.Reports.Include("User").OrderByDescending(x => x.Date).ToList();
            return View(reportsNow);
        }
        public IActionResult SortByVivodeship(string vivodeship)
        {
            var user = HttpContext.User.Identity.Name;
            var reportsNow = db.Reports.Include("User").Where(x => x.Vivodeship == vivodeship).OrderByDescending(x => x.Date).ToList();

            return View(reportsNow);
        }
    }
}