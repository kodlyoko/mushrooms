﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Mushrooms.Models;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace Mushrooms.Controllers
{
    [Authorize]
    public class MapController : Controller
    {
        private ApplicationDbContext db;
        public MapController(ApplicationDbContext Db)
        {
             db=Db;

        }
        public IActionResult Index()
        {
           
            var listofMarkers = new List<MarkerViewModel>();
            var placesFromDatabase = db.Places.Include("Report").ToList();
            foreach( var item in placesFromDatabase)
            {
                var ob = new MarkerViewModel
                {
                    lat = item.lat,
                    lng = item.lng,
                    info = item.Report.Description

                };
                listofMarkers.Add(ob);

            }

            string json = JsonConvert.SerializeObject(listofMarkers);
            ViewBag.Markers = json;

            return View();
        }
    }
}