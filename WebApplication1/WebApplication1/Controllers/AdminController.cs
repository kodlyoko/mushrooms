﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Mushrooms.Models;

namespace WebApplication1.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private UserManager<AppUser> userManager;
        private ApplicationDbContext app;
        public AdminController(UserManager<AppUser> userManage, ApplicationDbContext Db)
        {
            userManager = userManage;
            app = Db;
        }
        public IActionResult Index()
        {
            return View(userManager.Users);
        }
        [HttpPost]
        public async Task<IActionResult> Delete(string Id)
        {
            AppUser appUser = await userManager.FindByIdAsync(Id);
            if(appUser == null)
            {
                ModelState.AddModelError("", "Nie znaleziono użytkownika o podanym ID.");

            }
            else
            {
                IdentityResult result = await userManager.DeleteAsync(appUser);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    AddErrorsFromResult(result);

                }
            }
            return View("Index", userManager.Users);
        }

        private void AddErrorsFromResult(IdentityResult result)
        {
            foreach( var item in result.Errors)
            {
                ModelState.AddModelError("", item.Description);
            }
        }
    }
}