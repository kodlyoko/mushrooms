﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Mushrooms.Infrastructure
{
    public class CustomPasswordValidator :IPasswordValidator<AppUser>
    {
        public Task<IdentityResult> ValidateAsync(UserManager<AppUser> manager, AppUser user, string password)
        {
            List<string> passwordsForbidden = new List<string>();
            string[] forbidden = {"123456","123456789", "12345678", "12345", "111111","234567", "666666",
                "abc123","football","123123","654321","!@#$%^&*","aa123456","password1","qwerty123", "zaq12wsx"};
            passwordsForbidden.AddRange(forbidden);
            List<IdentityError> errors = new List<IdentityError>();
            foreach (var item in passwordsForbidden)
            {
                if (password.Contains(item))
                {
                    errors.Add(new IdentityError
                    {
                        Code = "",
                        Description = "Twoje hasło nie jest dozwolone."
                    });
                }
            }

            return Task.FromResult(errors.Count == 0 ? IdentityResult.Success : IdentityResult.Failed(errors.ToArray()));
        }

    }
}
