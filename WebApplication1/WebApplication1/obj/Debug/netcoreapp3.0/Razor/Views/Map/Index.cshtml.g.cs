#pragma checksum "C:\Users\agnie\OneDrive\Pulpit\Bitbucket\WebApplication1\WebApplication1\Views\Map\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "138bf8749d2b03fcbb3d0d1bf37bd8fde380f356"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Map_Index), @"mvc.1.0.view", @"/Views/Map/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\agnie\OneDrive\Pulpit\Bitbucket\WebApplication1\WebApplication1\Views\_ViewImports.cshtml"
using WebApplication1;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\agnie\OneDrive\Pulpit\Bitbucket\WebApplication1\WebApplication1\Views\_ViewImports.cshtml"
using WebApplication1.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"138bf8749d2b03fcbb3d0d1bf37bd8fde380f356", @"/Views/Map/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"729efaa87342638aecfe1a972ce9f9f8dff55b4c", @"/Views/_ViewImports.cshtml")]
    public class Views_Map_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Mushrooms.Models.Place>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\agnie\OneDrive\Pulpit\Bitbucket\WebApplication1\WebApplication1\Views\Map\Index.cshtml"
  
    ViewData["Title"] = "Widok Mapy";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<title>");
#nullable restore
#line 7 "C:\Users\agnie\OneDrive\Pulpit\Bitbucket\WebApplication1\WebApplication1\Views\Map\Index.cshtml"
  Write(ViewData["Title"]);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</title>

<script src=""https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js""></script>
<script src=""https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js""></script>
<fieldset>
    <legend style=""font-family: 'Segoe UI'; color: rgb(73,171,210); font-size: large;"">Widok dla raportów</legend>
    <meta charset=""utf-8"">
    <meta name=""viewport"" content=""width=device-width, initial-scale=1"">

    <div id=""googleMap"" style=""height:500px;width:100%;""></div>
    <script>
        var markers =");
#nullable restore
#line 18 "C:\Users\agnie\OneDrive\Pulpit\Bitbucket\WebApplication1\WebApplication1\Views\Map\Index.cshtml"
                Write(Html.Raw(ViewBag.Markers));

#line default
#line hidden
#nullable disable
            WriteLiteral(";\r\n        +66/1\r\n\r\n");
            WriteLiteral(@"        function AddSomeRandomMarkers() {

            var options = {
                zoom: 8,
                center: new google.maps.LatLng(49.8, 21.1), 
                mapTypeId: google.maps.MapTypeId.TERRAIN,
                mapTypeControl: false
            };

            // inicjalizacja mapy
            var map = new google.maps.Map(document.getElementById('googleMap'), options);

            var markersX = [];
             //ustawienie 
            for (var i = 0; i < markers.length; i++) {
                // init markers
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(markers[i].lat, markers[i].lng),
                    map: map,
                    title: 'Kliknij aby poznać szczegóły!' 
                });

                markersX.push(marker);
                // opisy dla dodawanych znaczników
                (function (marker, i) {

                    // dodanie evenu na click
                    google.map");
            WriteLiteral(@"s.event.addListener(marker, 'click', function () {
                        infowindow = new google.maps.InfoWindow({
                            content: markers[i].info
                        });
                        infowindow.open(map, marker);
                    });
                })(marker, i);
            }
            
                var markerCluster = new MarkerClusterer(map, markersX,
                    { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
         
        }
    </script>
    <script src=""https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js""></script>
        <script src=""https://maps.googleapis.com/maps/api/js?key=AIzaSyBuXfD40d0mvS29nMpQHBcR2CaWVT9fPhQ&callback=AddSomeRandomMarkers""></script>
</fieldset>
<footer>
    <p style=""background-color: rgb(73,171,210); font-weight: bold; color:white; text-align: center;height:50px; padding-top:15px"">© ");
#nullable restore
#line 73 "C:\Users\agnie\OneDrive\Pulpit\Bitbucket\WebApplication1\WebApplication1\Views\Map\Index.cshtml"
                                                                                                                                Write(DateTime.Now.ToLocalTime());

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n</footer>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Mushrooms.Models.Place>> Html { get; private set; }
    }
}
#pragma warning restore 1591
