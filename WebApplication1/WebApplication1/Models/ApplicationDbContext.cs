﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WebApplication1.Models;

namespace Mushrooms.Models
{
    public class ApplicationDbContext :DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) :base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
             .HasMany(c => c.Reports)
             .WithOne(e => e.User);

            modelBuilder.Entity<Place>()
                .HasOne<Report>(a => a.Report)
                .WithOne(s => s.Place)
                .HasForeignKey<Place>(a => a.UserRef);
            modelBuilder.Entity<AcheivementUser>()
            .HasKey(bc => new { bc.AcheivementId, bc.UserId });

            modelBuilder.Entity<AcheivementUser>()
           .HasOne(bc => bc.User)
           .WithMany(b => b.AcheivementUsers)
           .HasForeignKey(bc => bc.UserId);

            modelBuilder.Entity<AcheivementUser>()
                .HasOne(bc => bc.Acheivement)
                .WithMany(c => c.AcheivementUsers)
                .HasForeignKey(bc => bc.AcheivementId);

        }

        public DbSet<Place> Places { get; set; }
       public DbSet<User> Users { get; set; }
       public DbSet<Report> Reports { get; set; }
       public DbSet<Acheivement> Acheivements { get; set; }


    }
}
