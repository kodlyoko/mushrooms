﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace WebApplication1.Models
{
    public class AppIdentityDbContext : IdentityDbContext<AppUser>
    {
        public AppIdentityDbContext(DbContextOptions<AppIdentityDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Core Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Core Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
        public static async Task CreateAdminAccount(IServiceProvider sP, IConfiguration config)
        {
            UserManager <AppUser> uM = sP.GetRequiredService<UserManager<AppUser>>();
            RoleManager<IdentityRole> rM = sP.GetRequiredService<RoleManager<IdentityRole>>();

            string username = "Admin";
            string email = "admin@example.com";
            string password = "Sazxpl1";
            string role = "Admin";
            if(await uM.FindByNameAsync(username) ==null)
            {
                if(await rM.FindByNameAsync(role) == null)
                {
                    await rM.CreateAsync(new IdentityRole(role));
                }
                AppUser user = new AppUser
                {
                    UserName = username,
                    Email = email
                };
                IdentityResult result = await uM.CreateAsync(user, password);
                if(result.Succeeded)
                {
                    await uM.AddToRoleAsync(user, role);
                }

            }

        }
    }

}
