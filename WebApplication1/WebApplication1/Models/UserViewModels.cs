﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class CreateUserModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string PasswordConfirmed { get; set; }
    }
    public class LoginModel
    {
        [Required(ErrorMessage = "Proszę podać nazwę adres email.")]
        [UIHint("Adres email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Proszę podać hasło.")]
        [UIHint("hasło")]
        public string Password { get; set; }
    }
}
