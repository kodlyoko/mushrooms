﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mushrooms.Models
{
    public class MarkerViewModel
    {
        public double lat { get; set; }
        public double lng { get; set; }
        public string info { get; set; }

    }
}
