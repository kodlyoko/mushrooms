﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace Mushrooms.Models
{
    interface IRepository
    {
        IQueryable<User> Users { get; }
    }
}
