﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Mushrooms.Models
{
    public class Report
    {
        [Key]
        public int ReportID { get; set; }

        public User User { get; set; }

        public int UserId { get; set; }

        [Required(ErrorMessage = "Proszę wybrać województwo.")]
        public string Vivodeship { get; set; }

        [Required(ErrorMessage = "Proszę podać tytuł dla zgłoszenia.")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Proszę opisać zgłoszenie.")]
        public string Description { get; set; }
        [Required]
        [UIHint("Data dodania")]
        public DateTime Date { get; set; }
        [Range(typeof(bool), "true", "true", 
            ErrorMessage = "Zaakceptowanie regulaminu jest wymagane.")]
        public bool FieldAccepted { get; set; }

        public Place Place { get; set; }

        public string photoPath { get; set; }
    }
}
