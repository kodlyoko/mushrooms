﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Mushrooms.Models
{
    public class Acheivement
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<AcheivementUser> AcheivementUsers { get; set; }


    }
}
