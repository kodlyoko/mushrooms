﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Mushrooms.Models
{
    public class Place
    {
        [Key]
        public int Id { get; set; }
        public double lng { get; set; }
        public double lat { get; set; }
        public Report Report { get; set; }
        public int UserRef { get; set; }

    }
}
