﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Mushrooms.Models
{
    public class ReportViewModel
    {
        [Required(ErrorMessage = "Proszę wybrać województwo.")]
        [Display(Name = "Województwo")]
        public List<string> Vivodeship { get; set; }

        [Required(ErrorMessage = "Proszę podać tytuł dla zgłoszenia.")]
        [Display(Name = "Tytuł")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Proszę opisać zgłoszenie.")]
        [Display(Name = "Opis")]
        public string Description { get; set; }

        public DateTime Date { get; set; }
        [Range(typeof(bool), "true", "true", ErrorMessage = "Zaakceptowanie regulaminu jest wymagane.")]
        public bool FieldAccepted { get; set; }
        [Required]
        [Display(Name ="Długość geograficzna")]
        public double lat { get; set; }
        [Required]
        [Display(Name = "Szerokość geograficzna")]
        public double lng { get; set; }
        [Display(Name = "Zdjęcie")]
        public IFormFile Photo { get; set; }

    }
}
