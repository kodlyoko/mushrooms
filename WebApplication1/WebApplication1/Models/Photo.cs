﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Mushrooms.Models
{
    public class Photo
    {
        [Key]
        public int Id;
        public string Path;
    }
}
