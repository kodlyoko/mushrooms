﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace Mushrooms.Models
{
    public class EFRepository : IRepository
    {
        private ApplicationDbContext ctx;
        public EFRepository(ApplicationDbContext context)
        {
            ctx = context;
        }
        public IQueryable<User> Users
             => ctx.Users ;
    }
}
