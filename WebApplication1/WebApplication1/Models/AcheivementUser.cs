﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mushrooms.Models
{
    public class AcheivementUser
    {
        public int UserId { get; set; }
        public  User User { get; set; }
        public int AcheivementId { get; set; }
        public Acheivement Acheivement { get; set; }
    }
}
