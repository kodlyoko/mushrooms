﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Mushrooms.Migrations
{
    public partial class AcheivementUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Acheivements",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Acheivements", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AcheivementUser",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    AcheivementId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AcheivementUser", x => new { x.AcheivementId, x.UserId });
                    table.ForeignKey(
                        name: "FK_AcheivementUser_Acheivements_AcheivementId",
                        column: x => x.AcheivementId,
                        principalTable: "Acheivements",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AcheivementUser_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AcheivementUser_UserId",
                table: "AcheivementUser",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AcheivementUser");

            migrationBuilder.DropTable(
                name: "Acheivements");
        }
    }
}
