﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Mushrooms.Migrations
{
    public partial class photoPath : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "photoPath",
                table: "Reports",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "photoPath",
                table: "Reports");
        }
    }
}
